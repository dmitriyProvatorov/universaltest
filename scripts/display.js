/**
 * Created by Проваторов on 21.05.2014.
 */
function displayGligers(universalObjects){

    var cords = "";

    universalObjects.forEach(function(glider, index){

        cords += "glider " + (+index + 1) + " X: " + glider.x + " Y: " + glider.y + "<br>";
    });

    document.getElementById("dispGliders").innerHTML = cords;
}