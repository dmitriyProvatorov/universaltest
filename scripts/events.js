/**
 * Created by Проваторов on 21.05.2014.
 */
(function(window){

    if(document.addEventListener){

        document.addEventListener('click', routeEvents ,false);
    }
    else{
    
        document.attachEvent('onclick', routeEvents);
    }

    function routeEvents(event){

        event = event || window.event;

        if (event.preventDefault){
            event.preventDefault();
        } 
        else{
            event.returnValue = false;
        }

        if (event.stopPropagation){
            event.stopPropagation();
        }
        else{
            event.cancelBubble=true;
        }

        var target=event.target || event.srcElement;

        if(target.type == "button"){

            switch (target.value){

                case "create" :

                    create();
                    break;

                case "start" :

                    start();
                    break;

                case "stop" :

                    stop();
                    break;

                case "pause" :

                    pause();
                    break;

                case "proceed" :

                    proceed();
                    break;
            }
        }
    }

    function checkObj(name){

        return window[name];
    }

    function create() {

        if(!checkObj("myUniversal")){

            var width = parseInt(document.getElementById("width").value);
            var height = parseInt(document.getElementById("width").value);

            if (!isNaN(width) && !isNaN(height)){

                window.myUniversal = new Universal({

                    universalWidth: width,
                    universalHeight: height,

                    gliders: [
                        {
                            speed: 10,
                            vectorDeg: 0
                        },
                        {
                            speed: 20,
                            vectorDeg: 90
                        },
                        {
                            speed: 10,
                            vectorDeg: 180
                        },
                        {
                            speed: 20,
                            vectorDeg: 270
                        }
                    ]
                });
            }
            else {

                message("width or height universal is invalid");
            }
        }
        else{

            message("universal is already created");
        }

    }

    function start(){

        if(checkObj("myUniversal")){

            myUniversal.universalStart();
        }
        else{

            message("universal is not created");
        }
    }

    function stop(){

        if(checkObj("myUniversal")){

            myUniversal.universalStop();
        }
        else{

            message("universal is not created");
        }
    }

    function pause(){

        if(checkObj("myUniversal")){

            myUniversal.universalPause();
        }
        else{

            message("universal is not created");
        }
    }

    function proceed(){

        if(checkObj("myUniversal")){

            myUniversal.universalProceed();
        }
        else{

            message("universal is not created");
        }
    }

    function message(mes){

        alert(mes);
    }


})(window);