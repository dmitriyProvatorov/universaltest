/**
 * Created by Проваторов on 21.05.2014.
 */

// do not use the built-protected members of support for older browsers



function Universal(params){

    var universalWidth;
    var universalHeight;
    var universalCenterX;
    var universalCenterY;
    var universalObjects = [];
    var isUniversalCreate = false;
    var isUniversalObjectsCreate = false;
    var oldTime = false;
    var flagStart = false;
    var timeIteration = 1000; // time iteration universe

    function createUniversal(params){

        // create universal center

        universalWidth = params.universalWidth - 1;
        universalHeight = params.universalHeight - 1;
        universalCenterX = universalWidth / 2;
        universalCenterY = universalHeight / 2;
    }

    function checkUniversalParams(params){

        // check params universal is number аnd isNaN

        var listParams = ["universalWidth", "universalHeight"];
        var param;

        listParams.forEach( function(nameParam){

            param  = parseInt(params[nameParam]);

            if(!param  || isNaN(param)){

                throw "universal param " + nameParam + " is invalid";
            }

        });
    }

    function checkUniversalObjectsParams(params){

        var listParams = ["speed", "vectorDeg"];

        // check universal has gliders
        if(!params.gliders || !(params.gliders instanceof Array) || !params.gliders.length){

            throw "universal has not gliders";
        }

        var param;

        // check glider params
        params.gliders.forEach( function(glider, index){

            listParams.forEach( function(nameParam) {

                param = parseInt(glider[nameParam]);

                if (!param in glider || isNaN(param)){

                    throw "glider " + index + " param " + nameParam + " is invalid";
                }
            });
        });
    }

    function createUniversalObjectsParams(params){

        params.gliders.forEach( function(glider){

            glider.x = glider.x || universalCenterX;
            glider.y = glider.y || universalCenterY;

            universalObjects.push(glider);
        });
    }

    function moveUniversalObjects(){

        oldTime = oldTime || new Date()
        var newTime = new Date();
        var deltaTime = newTime - oldTime;		
        oldTime = newTime;

        universalObjects.forEach(function(glider, index){

            lianerMove(glider, index ,deltaTime);

            // check borders universe
            checkBorders(glider, index);
        });

        displayGligers(universalObjects);
        //message(universalObjects);
    }



    function  lianerMove(glider, index, deltaTime){    

		var deltaX;
		var deltaY;

        try{ 
				
                switch(glider.vectorDeg){
				
                        case 0:
                                deltaY = -glider.speed;
                                deltaX = 0;
                                break;
					
                        case 90:
                                deltaX = glider.speed;
                                deltaY = 0;
                                break;
					
                        case 180:
                                deltaY = glider.speed;
                                deltaX = 0;
                                break;
					
                        case 270:
                                deltaX = -glider.speed;
                                deltaY = 0;
                                break;
                }            

            glider.x += deltaX * (deltaTime/1000);
            glider.y += deltaY * (deltaTime/1000);;

        }
        catch(ex){

            clearAttributes();
            message("glider " + index + "coordinate calculation error " + ex);
        }
    }

    function checkBorders(glider, index){

        if( isNaN(glider.x)||
            isNaN(glider.y)||
            glider.x < 0 ||
            glider.y < 0 ||
            glider.x > universalWidth ||
            glider.y >  universalHeight){

            message("glider " + (+index + 1) + " left the universe");
        }
    }

    function message(mes){

        alert(mes);
    }

    function clearAttributes(){

        clearInterval(flagStart);
        flagStart = false;
        oldTime = false;
    }

    function checkUseStorage(){

        try {
            return 'localStorage' in window && window['localStorage'] !== null;
        } catch (e) {
            return false;
        }
    }

    function resetCoordinates(universalObjects){

        universalObjects.forEach(function(glider){

            glider.x = universalCenterX ;
            glider.y = universalCenterY;
        });
    }

    this.universalStart = function(){

        if(!flagStart){

            if (isUniversalCreate && isUniversalObjectsCreate){

                flagStart = setInterval(moveUniversalObjects, timeIteration);

                message("universe started");
            }
            else{

                message("error in universe or gliders");
            }
        }
        else{

            message("universe is already running");
        }
    };

    this.universalStop = function(){

       if(flagStart){

           clearAttributes();
           resetCoordinates(universalObjects);
           message("universe stopped");
       }
    };

    this.universalPause = function(){

        if(flagStart){

            clearAttributes();
            if(checkUseStorage()){

                localStorage.setItem("universalObjects", JSON.stringify(universalObjects));

                message("universalPaused");
            }
            else{

                message("universe stopped");
                message("browser has not support storage");
            }
        }
        else{

            message("universe is not running");
        }
    };

    this.universalProceed = function(){

        if(!flagStart){

            if (checkUseStorage()){

                var saveObjects = localStorage.getItem("universalObjects");

                if (saveObjects && isUniversalCreate && isUniversalObjectsCreate){

                    try{

                        universalObjects = JSON.parse(saveObjects);
                    }
                    catch(ex){

                        message("error storage object");
                        return;
                    }

                    flagStart = setInterval(moveUniversalObjects, timeIteration);
                    message("universalProceed");
                }
                else{

                    message("error storage object");
                }
            }
            else{

                message("universe started");
                message("browser has not support storage");
            }
        }
        else{

            message("stop before the universe");

        }
    };


    try{

        checkUniversalParams(params);
        createUniversal(params);
        checkUniversalObjectsParams(params);
        createUniversalObjectsParams(params);

        isUniversalCreate = true;
        isUniversalObjectsCreate = true;

        message("universal created");
    }
    catch(ex){

        clearAttributes();
        message(ex);
    }
}
